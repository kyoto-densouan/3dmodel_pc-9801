# README #

1/3スケールのNEC PC-9801風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1982年10月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-9800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801/raw/adafff6088b6a67e905b574c4bb55b114d22ec23/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801/raw/adafff6088b6a67e905b574c4bb55b114d22ec23/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801/raw/adafff6088b6a67e905b574c4bb55b114d22ec23/ExampleImage.jpg)
